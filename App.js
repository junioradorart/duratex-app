import React, { Fragment } from 'react';
import { ActivityIndicator, View, StyleSheet, Button, TouchableOpacity } from 'react-native';
import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './src/pages/HomeScreen';
import AppLogo from './src/components/general/AppLogo';
import { Font } from 'expo';
import { Entypo } from '@expo/vector-icons';
import { logoFontColor } from './src/constants/colors';
import { menuButtonMarginLeft } from './src/constants/pixel-perfect';
import GlobalLoader from './src/components/general/GlobalLoader';

const stackNavigator = createStackNavigator(
  {
    HomeScreen
  },
  {
    defaultNavigationOptions: {
      headerTitle: <AppLogo />,
      headerLeft: (data) => {
        return (
          <TouchableOpacity
            activeOpacity={.8}
            onPress={() => {data.scene.descriptor.navigation.toggleDrawer()}}
            style={{ marginLeft: menuButtonMarginLeft}}
          >
            <Entypo name='menu' color={logoFontColor} size={32} />
          </TouchableOpacity>
        )
      },
    }
  }
);

const appNavigator = createDrawerNavigator(
  {
    Home: stackNavigator
  }
);

const AppContainer = createAppContainer(appNavigator);

export default class App extends React.Component {
  state = {
    fontLoaded: false,
  };

  render() {
    return (
      <Fragment>
        { this.state.fontLoaded
          ? <AppContainer />
          : <GlobalLoader />
        }
      </Fragment>
    );
  }

  async componentDidMount() {
    await Font.loadAsync({
      'roboto-black': require('./assets/fonts/Roboto-Black.ttf'),
      'roboto-bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'roboto-italic': require('./assets/fonts/Roboto-Italic.ttf'),
      'roboto-light': require('./assets/fonts/Roboto-Light.ttf'),
      'roboto-medium': require('./assets/fonts/Roboto-Medium.ttf'),
      'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
    });

    this.setState({fontLoaded: true})
  }
}
