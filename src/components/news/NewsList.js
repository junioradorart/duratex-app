import React from 'react';
import { Text, StyleSheet, View, FlatList, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { primaryColor, white } from '../../constants/colors';
import {
  IMAGE_BORDER_RADIUS,
  LARGE_MARGING,
  MEDIUM_TITLE_FONT_SIZE,
  NEWS_TITLE_FONT_SIZE, SMALL_DESCRIPTION_FONT_SIZE
} from '../../constants/sizes';
import { defaultBoldTextsStyles, defaultOpacity, defaultRegularTextsStyles } from '../../constants/defaults';
import { thumbnailMarginBottom, titleMarginBottom } from '../../constants/pixel-perfect';

function NewsList(
  {
    listData = null,
    hasMarginBottom = false
  }
) {

  function renderItem({item}) {
    return (
        <TouchableOpacity
          activeOpacity={defaultOpacity}
          style={styles.item}
          onPress={() => {}}
        >
          <View>
            <Image
              style={styles.newsThumb}
              source={{uri: item.imageUrl}}
            />
            <Text style={styles.newsTitle}>{item.title}</Text>
            <Text numberOfLines={3} style={styles.newsDescription}>{item.description}</Text>
          </View>
        </TouchableOpacity>
    );
  }

  return (
    listData
      && (
        <View
          style={[
            styles.container,
            hasMarginBottom && {marginBottom: 45}
          ]}>
          <Text style={styles.sectionTitle}>{listData.sectionTitle}</Text>
          <FlatList
            horizontal={true}
            data={listData.data}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
      )
  );
}

NewsList.propTypes = {

};

const styles = StyleSheet.create({
  container: {
  },
  sectionTitle: {
    ...defaultBoldTextsStyles,
    color: primaryColor,
    fontSize: MEDIUM_TITLE_FONT_SIZE,
    marginBottom: LARGE_MARGING
  },
  newsTitle: {
    ...defaultBoldTextsStyles,
    color: primaryColor,
    fontSize: NEWS_TITLE_FONT_SIZE,
    marginBottom: titleMarginBottom,
    lineHeight: 22
  },
  newsThumb: {
    width: 222,
    height: 126,
    borderRadius: IMAGE_BORDER_RADIUS,
    marginBottom: thumbnailMarginBottom,
  },
  item: {
    marginRight: 10,
    maxWidth: 222
  },
  newsDescription: {
    ...defaultRegularTextsStyles,
    color: primaryColor,
    fontSize: SMALL_DESCRIPTION_FONT_SIZE,
  }
});

NewsList.propTypes = {
  listData: PropTypes.shape({
    sectionType: PropTypes.string,
    data: PropTypes.array
  }),
  hasMarginBottom: PropTypes.bool,
}

export default NewsList;
