import React, { Fragment } from 'react';
import { Text, Dimensions, View, TouchableOpacity, StyleSheet, ImageBackground, ActivityIndicator } from 'react-native';
import { defaultBoldTextsStyles, defaultOpacity, defaultRegularTextsStyles } from '../../constants/defaults';
import { white } from '../../constants/colors';
import { separatorHeight, separatorMarginBottom, separatorWidth, titleFontSize } from '../../constants/pixel-perfect';
import { NEWS_TITLE_FONT_SIZE } from '../../constants/sizes';
import GlobalLoader from '../general/GlobalLoader';
import PropTypes from 'prop-types';

function FeaturedNews(
  {
    isLoading = false,
    data = {}
  }
) {
  return (
      isLoading || !Object.keys(data)
        ? <GlobalLoader />
        : (
          <ImageBackground
            source={{uri: data.imageUrl}}
            style={styles.wrapper}
          >
            <TouchableOpacity
              activeOpacity={defaultOpacity}
              style={styles.container}
              onPress={() => {}}
            >
              <Text style={styles.title}>{ data.title }</Text>
              <View style={styles.separator} />
              <Text
                style={styles.descriptionText}
                ellipsizeMode='tail'
                numberOfLines={6}
              >{ data.description }</Text>
            </TouchableOpacity>
          </ImageBackground>
        )
  );
}

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: Dimensions.get('window').height,
  },
  container: {
    flex: 1,
    padding: 17,
    paddingTop: 70,
    maxHeight: '80%'
  },
  title: {
    ...defaultBoldTextsStyles,
    fontSize: titleFontSize,
    lineHeight: titleFontSize,
    color: white,
    marginBottom: 18
  },
  separator: {
    width: separatorWidth,
    height: separatorHeight,
    backgroundColor: white,
    marginBottom: separatorMarginBottom
  },
  descriptionText: {
    ...defaultRegularTextsStyles,
    fontSize: NEWS_TITLE_FONT_SIZE,
    lineHeight: 30,
    color: white
  }
});

FeaturedNews.propTypes = {
  isLoading: PropTypes.bool,
  data: PropTypes.shape({
    title: PropTypes.string,
    description: PropTypes.string,
    imageUrl: PropTypes.string
  })
};

export default FeaturedNews;
