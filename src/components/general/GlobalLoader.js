import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

function GlobalLoader() {
  return (
    <View style={styles.container}>
      <ActivityIndicator size='large' color='#0B4C69' />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center'
  }
});

export default GlobalLoader;
