import React from 'react';
import { Image, Text, View, StyleSheet } from 'react-native';
import { SMALL_MARGING } from '../../constants/sizes';
import { logoFontColor } from '../../constants/colors';

function AppLogo() {
  return (
    <View style={styles.container}>
      <Image
        source={require('../../../assets/logo-app.png')}
        style={{width: 110, height: 38}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    fontFamily: 'roboto-light',
    color: logoFontColor,
    fontSize: 30,
    marginRight: SMALL_MARGING
  }
});

export default AppLogo;
