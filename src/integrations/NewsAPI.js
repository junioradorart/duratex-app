import { duratexNewsList, featuredNews, inspirationsNewsList, topNewsList } from '../mocks/newsMockData';

export default class NewsAPI {
  static getFeaturedNews() {
    return new Promise((resolve, reject) => {
      resolve(featuredNews);
    });
  }

  static getTopNews() {
    return new Promise((resolve, reject) => {
      resolve(topNewsList);
    });
  }

  static getInspirationsNews() {
    return new Promise((resolve, reject) => {
      resolve(inspirationsNewsList);
    });
  }

  static getDuratexNews() {
    return new Promise((resolve, reject) => {
      resolve(duratexNewsList);
    });
  }
}
