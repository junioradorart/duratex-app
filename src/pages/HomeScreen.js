import React, { Fragment } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { LinearGradient } from 'expo';
import FeaturedNews from '../components/news/FeaturedNews';
import NewsAPI from '../integrations/NewsAPI';
import NewsList from '../components/news/NewsList';
import { white } from '../constants/colors';

export default class HomeScreen extends React.Component {
  state = {
    isFeaturedNewsLoading: true,
    featuredNewsData: {},
    topNewsData: {},
    inspirationsNewsData: {},
    duratexNewsData: {},
  };

  render() {
    return (
      <ScrollView >
        <FeaturedNews
          data={this.state.featuredNewsData}
          isLoading={this.state.isFeaturedNewsLoading}
        />
        <View style={styles.container}>
          <LinearGradient
            style={styles.linearGradientArea}
            colors={['transparent', 'rgba(255, 255, 255, .5)', white]}
            locations={[0, 0.05, 0.1]}
          >
            <NewsList hasMarginBottom listData={this.state.topNewsData} />
            <NewsList hasMarginBottom listData={this.state.inspirationsNewsData} />
            <NewsList listData={this.state.duratexNewsData} />
          </LinearGradient>
        </View>
      </ScrollView>
    );
  }

  componentDidMount() {
    NewsAPI.getFeaturedNews()
      .then(data => {
        this.setState(
          {
            featuredNewsData: data,
            isFeaturedNewsLoading: false,
          }
        );
      });

    NewsAPI.getTopNews()
      .then(data => {
        this.setState(
          {
            topNewsData: data,
          }
        );
      });

    NewsAPI.getInspirationsNews()
      .then(data => {
        this.setState(
          {
            inspirationsNewsData: data,
          }
        );
      });

    NewsAPI.getDuratexNews()
      .then(data => {
        this.setState(
          {
            duratexNewsData: data,
          }
        );
      });
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    flex: 1,
  },
  container: {
    marginTop: -250
  },
  linearGradientArea: {
    paddingLeft: 17,
    paddingTop: 50,
    paddingBottom: 50
  }
});
