export const featuredNews = {
  title: 'Faça parte do clube.',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vitae turpis elementum, ultrices elit dignissim, gravida augue.',
  imageUrl: 'https://dahabzone.com/1/wp-content/uploads/2018/09/Dahab-Paradise-dahab-zone-007.jpg'
};

export const newsListData = [
  {
    title: 'Loren Ipsum',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
    imageUrl: 'https://via.placeholder.com/222x126',
    id: '1'
  },
  {
    title: 'Loren Ipsum',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
    imageUrl: 'https://via.placeholder.com/222x126',
    id: '2'
  },
  {
    title: 'Loren Ipsum',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu ex ullamcorper, mattis ipsum facilisis, eleifend purus. Suspendisse malesuada sed ex vel cursus. Proin augue massa, lacinia quis magna non, ultrices vehicula mi. Ut vulputate tellus in egestas fermentum. Maecenas arcu ante, imperdiet sed ullamcorper at, rhoncus quis neque. Phasellus leo odio, malesuada sed mi sit amet, blandit ullamcorper tellus. Integer quis tortor tincidunt, mattis mauris nec, molestie sapien. Pellentesque eu maximus mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris gravida accumsan augue a sollicitudin. Cras non feugiat diam.',
    imageUrl: 'https://via.placeholder.com/222x126',
    id: '3'
  }
];

export const topNewsList = {
  sectionTitle: 'Destaques',
  data: newsListData
};

export const inspirationsNewsList = {
  sectionTitle: 'Ispirações',
  data: newsListData
};

export const duratexNewsList = {
  sectionTitle: 'Notícias da Duratex',
  data: newsListData
};

