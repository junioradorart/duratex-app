// menu
export const menuButtonMarginLeft = 13;

// FeaturedNews Component
export const titleFontSize = 57;
export const separatorWidth = 83;
export const separatorHeight = 2;
export const separatorMarginBottom = 22;

// NewsList
export const thumbnailMarginBottom = 17;
export const titleMarginBottom = 9;
