const SMALL_SIZE = 5;
const MEDIUM_SIZE = 10;
const LARGE_SIZE = 15;

export const IMAGE_BORDER_RADIUS = 5;

export const NEWS_TITLE_FONT_SIZE = 18;
export const SMALL_MARGING = SMALL_SIZE;
export const LARGE_MARGING = LARGE_SIZE;

export const MEDIUM_TITLE_FONT_SIZE = 28;
export const SMALL_DESCRIPTION_FONT_SIZE = 16;
