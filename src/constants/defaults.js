// texts
export const defaultBoldTextsStyles = {
  fontFamily: 'roboto-bold'
};

export const defaultRegularTextsStyles = {
  fontFamily: 'roboto-regular'
};

// TouchableOpacity
export const defaultOpacity = 0.8
