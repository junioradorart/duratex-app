# Duratex App Test

## Instalação
Após clonar este repositório use `npm install` para instalar as dependências.

## Rodar
Para rodar o projeto basta usar o comando `npm start`, e o servidor do expo será iniciado e você poderá usar o app do expo em seu celular para acessar via QRCODE

## OBSERVAÇÕES GERAIS
Tentei ser o mais fiel possível ao layout que me foi passado. Porém alguns ítens não puderam ser fieis por exemplo: As fontes usadas que no caso são pagas e as imagens usadas.
No caso da Font, usei uma outra 'custom font' que é a Roboto.